# Lab 8 - Exploratory testing and UI
 I choose [kinopoisk](https://kinopoisk.ru)

| Test Tour           | test-1                   |
|---------------------|--------------------------|
| Object to be tested | Test recommendation list |
| Test Duration       | 15 s                     |
| Tester              | Grigorii Kostarev        |

| N   | What done                            | Status             | Comment |
|-----|--------------------------------------|--------------------|---------|
| 1   | Press button "Films"                 | :white_check_mark: |         |
| 2   | Choose top playlist "250 best films" | :white_check_mark: |         |
| 3   | Choose top film in the list          | :white_check_mark: |         |
| 4   | Redirect to film page                | :white_check_mark: |         |
| 5   | Name of film is "The Green Mile"     | :white_check_mark: |         |
| 6   | Press KINOPOISK button               | :white_check_mark: |         |
| 7   | Redirect on main page                | :white_check_mark: |         |

---

| Test Tour           | test-1            |
|---------------------|-------------------|
| Object to be tested | Test search       |
| Test Duration       | 30 s              |
| Tester              | Grigorii Kostarev |

| N   | What done                        | Status             | Comment |
|-----|----------------------------------|--------------------|---------|
| 1   | Press search line                | :white_check_mark: |         |
| 2   | Write "?"                        | :white_check_mark: |         |
| 3   | Verify that no films             | :white_check_mark: |         |
| 4   | Delete symbol                    | :white_check_mark: |         |
| 5   | Write "The Green Mile"           | :white_check_mark: |         |
| 6   | Choose top film in the list      | :white_check_mark: |         |
| 7   | Redirect to film page            | :white_check_mark: |         |
| 8   | Name of film is "The Green Mile" | :white_check_mark: |         |
| 9   | Press KINOPOISK button           | :white_check_mark: |         |
| 10  | Redirect on main page            | :white_check_mark: |         |

---

| Test Tour           | test-1               |
|---------------------|----------------------|
| Object to be tested | Test tickets service |
| Test Duration       | 35 s                 |
| Tester              | Grigorii Kostarev    |

| N   | What done                          | Status             | Comment |
|-----|------------------------------------|--------------------|---------|
| 1   | Press button "Movie tickets"       | :white_check_mark: |         |
| 2   | Wait page loading                  | :white_check_mark: |         |
| 3   | Press dropdown list near to "Film" | :white_check_mark: |         |
| 4   | Pick "John Wick: Chapter 4"        | :white_check_mark: |         |
| 5   | Press in any place to close list   | :white_check_mark: |         |
| 6   | Press button "Show schedule"       | :white_check_mark: |         |
| 7   | Wait page loading                  | :white_check_mark: |         |
| 8   | Press search line                  | :white_check_mark: |         |
| 9   | Write "Innopolis"                  | :white_check_mark: |         |
| 10  | Verify that result is empty list   | :white_check_mark: |         |
| 11  | Press KINOPOISK button             | :white_check_mark: |         |
| 12  | Redirect on main page              | :white_check_mark: |         |


package com.example.lab8;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

@SpringBootTest
class KinopoiskTest {

    private WebDriver webDriver;

    @BeforeEach
    void init() {
        System.setProperty("webdriver.chrome.driver", "/Users/grishakostarev/Downloads/chromedriver_mac_arm64/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        webDriver = new ChromeDriver(options);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.get("https://www.kinopoisk.ru");
    }

    @AfterEach
    void tearDown() {
        webDriver.quit();
    }

    @Test
    void bestMovieTest() {
        // Click the "Navigation" button
        WebElement sideBar = webDriver.findElement(By.xpath("//button[@class='styles_root__coHaQ styles_burger__HcbjK']"));
        sideBar.click();

        // Click the "Films" button
        WebElement filmsButton = webDriver.findElement(By.xpath("//a[@href='/lists/categories/movies/1/' and @class='styles_root__7mPJN styles_darkThemeItem__E_aGY' and @data-tid='de7c6530']"));
        filmsButton.click();

        // Click the "250 best films" playlist
        WebElement bestFilmsPlaylist = webDriver.findElement(By.xpath("//a[@href='/lists/movies/top250/']"));
        bestFilmsPlaylist.click();

        // Click the top film in the list
        WebElement topFilm = webDriver.findElement(By.xpath("//div[@class='base-movie-main-info_mainInfo__ZL_u3']/span[1]"));
        topFilm.click();

        // Verify that the film is "The Green Mile"
        WebElement filmTitle = webDriver.findElement(By.xpath("//h1[@class='styles_title__65Zwx styles_root__l9kHe styles_root__5sqsd styles_rootInDark__SZlor']/span[1]"));
        String expectedTitle = "Зеленая миля (1999)";
        String actualTitle = filmTitle.getText();
        if (!actualTitle.equals(expectedTitle)) {
            System.out.println("Error: The film title is not 'The Green Mile'");
        }

        // Click the "KINOPOISK" button
        WebElement kinopoiskButton = webDriver.findElement(By.xpath("//a[@class='styles_root__dYidr styles_logo___bcop']"));
        kinopoiskButton.click();

        // Navigate back to the main page
        webDriver.navigate().back();
    }

}
